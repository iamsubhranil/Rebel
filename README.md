# Rebel
A rebelious virtual machine

Prolouge
========

1. At the time of this writing, 23rd of March, 2018, 09:33 PM IST, this machine is solely intended to be *just a virtual machine*. 

2. It should provide a strong interface to support a variety of dynamically typed languages, with full support for object oriented programming 
and garbage collection. 

3. Since this machine will execute the bytecode, it will define a standardized representation for values, which should be abstract enough to be tweaked as per the requirement of a particular language. 

4. This machine will try to assume as less as it can about the syntax of the source code and the implementation of the compiler, and to concentrate solely on performing execution in a standardized execution model. 

5. There should be couple of headers(Ideally, just _one_), which should be included on the language to have the vm run its bytecode, in as painless manner as possible. However, performance will be always prioritized over simplicity. 

6. Ideally, there should be specific macros which will disable object orientation and/or garbage collection of the machine, making it suitable to execute just about any language out there, but also providing full support for those who need it.
